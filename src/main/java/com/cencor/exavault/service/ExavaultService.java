package com.cencor.exavault.service;

import java.io.UnsupportedEncodingException;

/**
 * @author AJHM
 */
public interface ExavaultService {
    /**
     * Bajar archivo de Exavault.
     *
     * @param apiKey       api key
     * @param user         usuario
     * @param password     password
     * @param filePath     path
     * @param downloadName nombre con el que se va a bajar
     * @return {@link byte}
     * @throws UnsupportedEncodingException unsupportedEncodingException
     */
    byte[] downloadFile(String apiKey, String user, String password, String filePath, String downloadName)
            throws UnsupportedEncodingException;

    /**
     * Subir archivo de Exavault.
     *
     * @param apiKey          api key
     * @param user            usuario
     * @param password        password
     * @param file            archivo
     * @param destinationPath path
     * @param overwrite       si se puede sobreescribir
     * @throws UnsupportedEncodingException unsupportedEncodingException
     */
    void uploadFile(String apiKey, String user, String password, byte[] file, String destinationPath, boolean overwrite)
            throws UnsupportedEncodingException;
}
