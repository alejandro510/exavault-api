package com.cencor.exavault.service;

import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.file.dto.UrlResult;
import com.cencor.exavault.file.service.FileService;
import com.cencor.exavault.security.dto.Login;
import com.cencor.exavault.security.service.AuthenticationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * @author AJHM
 */
@Component
public class ExavaultServiceImpl implements ExavaultService {
    /**
     * Servicio de logueo.
     */
    private AuthenticationService authenticationService;
    /**
     * Servicio de archivos.
     */
    private FileService fileService;

    /**
     * Constructor.
     *
     * @param authenticationService {@link AuthenticationService}
     * @param fileService           {@link FileService}
     */
    @Autowired
    public ExavaultServiceImpl(final AuthenticationService authenticationService, final FileService fileService) {
        this.authenticationService = authenticationService;
        this.fileService = fileService;
    }

    /**
     * Bajar archivo de Exavault.
     *
     * @param apiKey       api key
     * @param user         usuario
     * @param password     password
     * @param filePath     path
     * @param downloadName nombre con el que se va a bajar
     * @return {@link byte}
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public final byte[] downloadFile(
            final String apiKey, final String user, final String password,
            final String filePath, final String downloadName) throws UnsupportedEncodingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Response responseLogin = authenticationService.login(apiKey, user, password);
        String accessToken = (objectMapper.convertValue(responseLogin.getResults(), Login.class)).getAccessToken();

        Response responseUrl = fileService.getDownloadUrl(apiKey, accessToken, filePath, downloadName);
        String url = (objectMapper.convertValue(responseUrl.getResults(), UrlResult.class)).getUrl();

        byte[] file = fileService.getFile(url);

        authenticationService.logout(apiKey, accessToken);

        return file;
    }

    /**
     * Subir archivo de Exavault.
     *
     * @param apiKey          api key
     * @param user            usuario
     * @param password        password
     * @param file            archivo
     * @param destinationPath path
     * @param overwrite       si se puede sobreescribir
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public final void uploadFile(
            final String apiKey, final String user, final String password, final byte[] file,
            final String destinationPath, final boolean overwrite) throws UnsupportedEncodingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Response responseLogin = authenticationService.login(apiKey, user, password);
        String accessToken = (objectMapper.convertValue(responseLogin.getResults(), Login.class)).getAccessToken();

        Response responseUrl = fileService.getUploadUrl(apiKey, accessToken, file.length, destinationPath, overwrite);
        String url = (objectMapper.convertValue(responseUrl.getResults(), UrlResult.class)).getUrl();

        fileService.setFile(url, file);


        authenticationService.logout(apiKey, accessToken);
    }
}
