package com.cencor.exavault.common.dto;

/**
 * @author AJHM
 */
public class ErrorResponse {
    /**
     * Mensaje.
     */
    private String message;
    /**
     * Codigo de error.
     */
    private int code;

    /**
     * @param message {@link ErrorResponse#message}
     */
    public final void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return {@link ErrorResponse#message}
     */
    public final String getMessage() {
        return message;
    }

    /**
     * @param code {@link ErrorResponse#code}
     */
    public final void setCode(final int code) {
        this.code = code;
    }

    /**
     * @return {@link ErrorResponse#code|}
     */
    public final int getCode() {
        return code;
    }
}
