package com.cencor.exavault.common.dto;

/**
 * @author AJHM
 */
public class Response {
    /**
     * Codigo de exito.
     */
    private int success;
    /**
     * Objeto con el resultado.
     */
    private Object results;
    /**
     * Objecto de Error.
     */
    private ErrorResponse error;

    /**
     * @param success {@link Response#success}
     */
    public final void setSuccess(final int success) {
        this.success = success;
    }

    /**
     * @return {@link Response#success}
     */
    public final int getSuccess() {
        return success;
    }

    /**
     * @param results {@link Response#results}
     */
    public final void setResults(final Object results) {
        this.results = results;
    }

    /**
     * @return {@link Response#results}
     */
    public final Object getResults() {
        return results;
    }

    /**
     * @param error {@link Response#error}
     */
    public final void setError(final ErrorResponse error) {
        this.error = error;
    }

    /**
     * @return {@link Response#error}
     */
    public final ErrorResponse getError() {
        return error;
    }
}
