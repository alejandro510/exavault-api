package com.cencor.exavault.common.exception;

/**
 * @author AJHM
 */
public class FileNotFoundException extends RuntimeException {
    /**
     * Constructor.
     *
     * @param fileName nombre del archivo
     */
    public FileNotFoundException(final String fileName) {
        super("El archivo " + fileName + " no se encuentra en la ruta.");
    }
}