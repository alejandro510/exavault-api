package com.cencor.exavault.common.exception;

/**
 * @author AJHM
 */
public class AuthenticationFailException extends RuntimeException {
    /**
     * Constructor.
     *
     * @param cause Causa de la excepcion
     */
    public AuthenticationFailException(final String cause) {
        super("Error al autentificarse a Exavault, " + cause + ".");
    }
}
