package com.cencor.exavault.file.dto;

/**
 * @author AJHM
 */
public class UrlResult {
    /**
     * Url para subir o bajar los archivos.
     */
    private String url;
    /**
     * offset.
     */
    private int offset;

    /**
     * @return {@link UrlResult#url}
     */
    public final String getUrl() {
        return url;
    }

    /**
     * @param url @return {@link UrlResult#url}
     */
    public final void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return {@link UrlResult#offset}
     */
    public final int getOffset() {
        return offset;
    }

    /**
     * @param offset {@link UrlResult#offset}
     */
    public final void setOffset(final int offset) {
        this.offset = offset;
    }
}
