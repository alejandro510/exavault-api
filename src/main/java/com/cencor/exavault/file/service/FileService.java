package com.cencor.exavault.file.service;

import com.cencor.exavault.common.dto.Response;

import java.io.UnsupportedEncodingException;

/**
 * @author AJHM
 */
public interface FileService {
    /**
     * Obtiene la URL para descargar los archivos.
     *
     * @param apiKey       api key
     * @param accessToken  token de login
     * @param filePath     ruta de archivo, debe de contener el nombre del archivo
     * @param downloadName nombre con el que se bajara el archivo
     * @return {@link Response}
     */
    Response getDownloadUrl(String apiKey, String accessToken, String filePath, String downloadName);

    /**
     * Obtiene la URL para subir los archivos.
     *
     * @param apiKey          api key
     * @param accessToken     token de login
     * @param fileSize        tamaño de archivo
     * @param destinationPath ruta de archivo, debe de contener el nombre del archivo
     * @param overwrite       si puede sobreescribirse
     * @return {@link Response}
     */
    Response getUploadUrl(String apiKey, String accessToken, int fileSize, String destinationPath, boolean overwrite);

    /**
     * Bajar el archivo apartir de una ruta.
     *
     * @param url url para bajar el archivo
     * @return {@link byte}
     * @throws UnsupportedEncodingException {@link UnsupportedEncodingException}
     */
    byte[] getFile(String url) throws UnsupportedEncodingException;

    /**
     * Sube el archivo a Exavault.
     *
     * @param url       url para bajar el archivo
     * @param byteArray arreglo de bytes del archivo
     * @return {@link int}
     * @throws UnsupportedEncodingException {@link UnsupportedEncodingException}
     */
    int setFile(String url, byte[] byteArray) throws UnsupportedEncodingException;
}
