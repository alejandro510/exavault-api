package com.cencor.exavault.file.service;

import com.cencor.exavault.common.dto.ErrorResponse;
import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.common.exception.FileNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;

/**
 * @author AJHM
 */
public class FileServiceImpl implements FileService {
    /**
     * Error en exavault.
     */
    private static final int ERROR_FILE_NOT_FOUND = 450;
    /**
     * Url para obtener url.
     */
    @Value("${EXAVAULT_REST_GET_URL_DOWNLOAD}")
    private String downloadUrl;

    /**
     * Url para obtener url.
     */
    @Value("${EXAVAULT_REST_GET_URL_UPLOAD}")
    private String uploadUrl;

    /**
     * Obtiene la URL para descargar los archivos.
     *
     * @param apiKey       api key
     * @param accessToken  token de login
     * @param filePath     ruta de archivo, debe de contener el nombre del archivo
     * @param downloadName nombre con el que se bajara el archivo
     * @return {@link Response}
     */
    public final Response getDownloadUrl(
            final String apiKey, final String accessToken, final String filePath, final String downloadName) {
        RestTemplate restTemplate = new RestTemplate();

        URI target = UriComponentsBuilder.fromUriString(downloadUrl)
                .queryParam("api_key", apiKey)
                .queryParam("access_token", accessToken)
                .queryParam("filePaths", filePath)
                .queryParam("downloadName", downloadName)
                .build()
                .encode()
                .toUri();

        Response response = restTemplate.getForObject(target, Response.class);

        if (response.getError() != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            if ((objectMapper.convertValue(
                    response.getError(), ErrorResponse.class)).getCode() == ERROR_FILE_NOT_FOUND) {
                throw new FileNotFoundException(filePath);
            }
        }

        return response;
    }

    /**
     * Obtiene la URL para subir los archivos.
     *
     * @param apiKey          api key
     * @param accessToken     token de login
     * @param fileSize        tamaño de archivo
     * @param destinationPath ruta de archivo, debe de contener el nombre del archivo
     * @param overwrite       si puede sobreescribirse
     * @return {@link Response}
     */
    public final Response getUploadUrl(
            final String apiKey, final String accessToken, final int fileSize,
            final String destinationPath, final boolean overwrite) {
        RestTemplate restTemplate = new RestTemplate();

        URI target = UriComponentsBuilder.fromUriString(uploadUrl)
                .queryParam("api_key", apiKey)
                .queryParam("access_token", accessToken)
                .queryParam("fileSize", String.valueOf(fileSize))
                .queryParam("destinationPath", destinationPath)
                .queryParam("allowOverwrite", String.valueOf(overwrite))
                .queryParam("resume", String.valueOf(false))
                .build()
                .encode()
                .toUri();

        return restTemplate.getForObject(target, Response.class);
    }

    /**
     * Obtener archivo.
     *
     * @param url url para bajar el archivo
     * @return {@link byte}
     * @throws UnsupportedEncodingException {@link UnsupportedEncodingException}
     */
    public final byte[] getFile(final String url) throws UnsupportedEncodingException {
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.getForObject(URLDecoder.decode(url, "UTF-8"), byte[].class);
    }

    /**
     * Sube el archivo a Exavault.
     *
     * @param url       url para bajar el archivo
     * @param byteArray arreglo de bytes del archivo
     * @return {@link int}
     * @throws UnsupportedEncodingException {@link UnsupportedEncodingException}
     */
    public final int setFile(final String url, final byte[] byteArray) throws UnsupportedEncodingException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        Resource file = new ByteArrayResource(byteArray);
        body.add("file", file);

        return restTemplate.postForEntity(
                URLDecoder.decode(url, "UTF-8"), file, String.class).getStatusCode().value();

    }
}
