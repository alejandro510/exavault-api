package com.cencor.exavault.security.dto;

/**
 * @author AJHM
 */
public class Login {
    /**
     * Usuario.
     */
    private String username;
    /**
     * token de acceso.
     */
    private String accessToken;
    /**
     * Modo.
     */
    private int mode;
    /**
     * Ip del Cliente.
     */
    private String clientIp;

    /**
     * @param username {@link Login#username}
     */
    public final void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return {@link Login#username}
     */
    public final String getUsername() {
        return username;
    }

    /**
     * @param accessToken {@link Login#accessToken}
     */
    public final void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * @return {@link Login#accessToken}
     */
    public final String getAccessToken() {
        return accessToken;
    }

    /**
     * @return {@link Login#mode}
     */
    public final int getMode() {
        return mode;
    }

    /**
     * @param mode {@link Login#mode}
     */
    public final void setMode(final int mode) {
        this.mode = mode;
    }

    /**
     * @return {@link Login#clientIp}
     */
    public final String getClientIp() {
        return clientIp;
    }

    /**
     * @param clientIp {@link Login#clientIp}
     */
    public final void setClientIp(final String clientIp) {
        this.clientIp = clientIp;
    }
}
