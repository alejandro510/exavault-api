package com.cencor.exavault.security.service;

import com.cencor.exavault.common.dto.Response;

/**
 * @author AJHM
 */
public interface AuthenticationService {
    /**
     * Servicio para logueo.
     *
     * @param apiKey   api key de exavault
     * @param user     usuario
     * @param password password
     * @return {@link Response}
     */
    Response login(String apiKey, String user, String password);

    /**
     * Servicio de deslogueo.
     *
     * @param apiKey     api key de exavault
     * @param tokenLogin token de logueo
     * @return {@link Response}
     */
    Response logout(String apiKey, String tokenLogin);
}
