package com.cencor.exavault.security.service;

import com.cencor.exavault.common.dto.ErrorResponse;
import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.common.exception.AuthenticationFailException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * @author AJHM
 */
@Component
public class AuthenticationServiceImpl implements AuthenticationService {
    /**
     * Error en exavault.
     */
    private static final int ERROR_CODE_API = 250;
    /**
     * Error en exavault.
     */
    private static final int ERROR_CODE_USER_PASSWORD = 200;

    /**
     * Url de logueo.
     */
    @Value("${EXAVAULT_REST_LOGIN}")
    private String urlLogin;
    /**
     * Url de deslogueo.
     */
    @Value("${EXAVAULT_REST_LOGOUT}")
    private String urlLogout;

    /**
     * Logueo.
     *
     * @param apiKey   api key de exavault
     * @param user     usuario
     * @param password password
     * @return {@link Response}
     */
    public final Response login(final String apiKey, final String user, final String password) {
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("api_key", apiKey);
        parameters.add("username", user);
        parameters.add("password", password);

        Response response = restTemplate.postForObject(urlLogin, parameters, Response.class);

        if (response.getError() != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            if ((objectMapper.convertValue(response.getError(), ErrorResponse.class)).getCode() == ERROR_CODE_API) {
                throw new AuthenticationFailException("Api Key Incorrecto");
            }

            if ((objectMapper.convertValue(response.getError(), ErrorResponse.class)).getCode()
                    == ERROR_CODE_USER_PASSWORD) {
                throw new AuthenticationFailException("usuario o password incorrectos");
            }
        }

        return response;
    }

    /**
     * Deslogueo.
     *
     * @param apiKey      api key de exavault
     * @param accessToken token de logueo
     * @return {@link Response}
     */
    public final Response logout(final String apiKey, final String accessToken) {
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("api_key", apiKey);
        parameters.add("access_token", accessToken);

        return restTemplate.postForObject(urlLogout, parameters, Response.class);
    }
}
