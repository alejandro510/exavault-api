package com.cencor.exavault.file.service;

import com.cencor.exavault.common.dto.ErrorResponse;
import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.file.dto.UrlResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class UploadServiceTest {
    @Mock
    private FileService fileService;

    private Response uploadUrlResponse;
    private Response badUploadUrlResponse;

    @Before
    public void init() {
        uploadUrlResponse = new Response();
        uploadUrlResponse.setSuccess(1);
        UrlResult urlResult = new UrlResult();
        urlResult.setUrl("urlUpload");
        urlResult.setOffset(0);
        uploadUrlResponse.setResults(urlResult);


        badUploadUrlResponse = new Response();
        badUploadUrlResponse.setSuccess(0);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode(310);
        errorResponse.setMessage("Access token 'accessToken' not found");
        badUploadUrlResponse.setError(errorResponse);
    }

    @Test
    public void getUploadUrlFileTest() {
        BDDMockito.when(fileService.getUploadUrl("apiKey", "accessToken", 0, "destinationPath", true)).thenReturn(uploadUrlResponse);

        Response response = fileService.getUploadUrl("apiKey", "accessToken", 0, "destinationPath", true);

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        UrlResult urlResult = objectMapper.convertValue(response.getResults(), UrlResult.class);

        Assert.assertNotNull(urlResult);
        Assert.assertEquals(1, response.getSuccess());
        Assert.assertEquals("urlUpload", urlResult.getUrl());
        Assert.assertEquals(0, urlResult.getOffset());
    }

    @Test
    public void errorGetDownloadUrlFileTest() {
        BDDMockito.when(fileService.getUploadUrl("apiKey", "accessToken", 0, "destinationPath", true)).thenReturn(badUploadUrlResponse);

        Response response = fileService.getUploadUrl("apiKey", "accessToken", 0, "destinationPath", true);

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        ErrorResponse errorResponse = objectMapper.convertValue(response.getError(), ErrorResponse.class);

        Assert.assertNotNull(errorResponse);
        Assert.assertEquals(0, response.getSuccess());
        Assert.assertEquals("Access token 'accessToken' not found", errorResponse.getMessage());
    }

    @Test
    public void uploadFile() throws IOException {
        BDDMockito.when(fileService.setFile("url", "uploadFile".getBytes())).thenReturn(200);

        Assert.assertEquals(200, fileService.setFile("url", "uploadFile".getBytes()));
    }
}
