package com.cencor.exavault.file.service;

import com.cencor.exavault.common.dto.ErrorResponse;
import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.file.dto.UrlResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.UnsupportedEncodingException;

@RunWith(MockitoJUnitRunner.class)
public class DownloadServiceTest {
    @Mock
    private FileService fileService;

    private Response downloadUrlResponse;
    private Response badDownloadUrlResponse;

    @Before
    public void init() {
        downloadUrlResponse = new Response();
        downloadUrlResponse.setSuccess(1);
        UrlResult urlResult = new UrlResult();
        urlResult.setUrl("urlDownload");
        downloadUrlResponse.setResults(urlResult);


        badDownloadUrlResponse = new Response();
        badDownloadUrlResponse.setSuccess(0);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode(310);
        errorResponse.setMessage("Access token 'accessToken' not found");
        badDownloadUrlResponse.setError(errorResponse);
    }

    @Test
    public void getDownloadUrlFileTest() {
        BDDMockito.when(fileService.getDownloadUrl("apiKey", "accessToken", "filePath", "downloadName")).thenReturn(downloadUrlResponse);

        Response response = fileService.getDownloadUrl("apiKey", "accessToken", "filePath", "downloadName");

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        UrlResult urlResult = objectMapper.convertValue(response.getResults(), UrlResult.class);

        Assert.assertNotNull(urlResult);
        Assert.assertEquals(1, response.getSuccess());
        Assert.assertEquals("urlDownload", urlResult.getUrl());
    }

    @Test
    public void errorGetDownloadUrlFileTest() {
        BDDMockito.when(fileService.getDownloadUrl("apiKey", "accessToken", "filePath", "downloadName")).thenReturn(badDownloadUrlResponse);

        Response response = fileService.getDownloadUrl("apiKey", "accessToken", "filePath", "downloadName");

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        ErrorResponse errorResponse = objectMapper.convertValue(response.getError(), ErrorResponse.class);

        Assert.assertNotNull(errorResponse);
        Assert.assertEquals(0, response.getSuccess());
        Assert.assertEquals("Access token 'accessToken' not found", errorResponse.getMessage());
    }

    @Test
    public void downloadFile() throws UnsupportedEncodingException {
        byte[] fileRequest = "downloadFile".getBytes();

        BDDMockito.when(fileService.getFile("url")).thenReturn(fileRequest);

        byte[] fileRequestTest = fileService.getFile("url");

        Assert.assertNotNull(fileRequestTest);

        Assert.assertEquals("downloadFile", new String(fileRequestTest));
    }
}
