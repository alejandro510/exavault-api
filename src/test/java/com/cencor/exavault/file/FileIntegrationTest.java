package com.cencor.exavault.file;

import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.file.dto.UrlResult;
import com.cencor.exavault.file.service.FileService;
import com.cencor.exavault.file.service.FileServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FileServiceImpl.class})
@AutoConfigureWireMock(port = 0)
public class FileIntegrationTest {
    @Value("${wiremock.server.port}")
    private String port;

    @Autowired
    FileService fileService;

    @Test
    public void getDownloadUrlFileTest() {
        WireMock.stubFor(
                WireMock.get(
                        WireMock.urlEqualTo("/v1/getDownloadFileUrl?api_key=apiKey&access_token=accessToken&filePaths=filePath&downloadName=downloadName"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": {\"url\": \"url\"}}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        Response response = fileService.getDownloadUrl("apiKey", "accessToken", "filePath", "downloadName");

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        UrlResult urlResultTest = objectMapper.convertValue(response.getResults(), UrlResult.class);

        Assert.assertNotNull(urlResultTest);
        Assert.assertEquals(1, response.getSuccess());
        Assert.assertEquals("url", urlResultTest.getUrl());
    }

    @Test
    public void getUploadUrlFileTest() {
        WireMock.stubFor(
                WireMock.get(
                        WireMock.urlEqualTo("/v1/getUploadFileUrl?api_key=apiKey&access_token=accessToken&fileSize=0&destinationPath=destinationPath&allowOverwrite=true&resume=false"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": {\"url\": \"url\"}}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        Response response = fileService.getUploadUrl("apiKey", "accessToken", 0, "destinationPath", true);

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        UrlResult urlResultTest = objectMapper.convertValue(response.getResults(), UrlResult.class);

        Assert.assertNotNull(urlResultTest);
        Assert.assertEquals(1, response.getSuccess());
        Assert.assertEquals("url", urlResultTest.getUrl());
    }

    @Test
    public void downloadFileTest() throws UnsupportedEncodingException {

        WireMock.stubFor(
                WireMock.get(
                        WireMock.urlEqualTo("/v1/download?access_token=5bbceed54911e6c06b16825019a6b7249dc06b71ab2b8&filePaths=Report/SecLen_SURA_20181005.xml&downloadName=SecLen_SURA_20181005.xml"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withBody("downloadFile".getBytes())
                                        .withHeader("Content-Type", "application/octet-stream")

                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        byte[] file = fileService.getFile(
                "http://localhost:" +
                        port +
                        "/v1/download?access_token=5bbceed54911e6c06b16825019a6b7249dc06b71ab2b8&filePaths=Report%2FSecLen_SURA_20181005.xml&downloadName=SecLen_SURA_20181005.xml");

        Assert.assertNotNull(file);

        Assert.assertEquals("downloadFile", new String(file));
    }

    @Test
    public void uploadFileTest() throws IOException {

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/upload?access_token=5bbcf504ee5fd4bd6ef3b15d946a58c18bd5ce0e9177e&destinationPath=Prueba_SURA.txt&fileSize=0&allowOverwrite=1&resume=0"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withBody("")
                                        .withHeader("Content-Type", "application/octet-stream")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        Assert.assertEquals(200, fileService.setFile(
                "http://localhost:" +
                        port +
                        "/v1/upload?access_token=5bbcf504ee5fd4bd6ef3b15d946a58c18bd5ce0e9177e&destinationPath=Prueba_SURA.txt&fileSize=0&allowOverwrite=1&resume=0", "uploadFile".getBytes()));


    }

    @Test
    public void uploadFileNotOverwriteTest() throws IOException {

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/upload?access_token=5bbcf504ee5fd4bd6ef3b15d946a58c18bd5ce0e9177e&destinationPath=Prueba_SURA.txt&fileSize=0&allowOverwrite=0&resume=0"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withBody("")
                                        .withHeader("Content-Type", "application/octet-stream")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        Assert.assertEquals(200, fileService.setFile(
                "http://localhost:" +
                        port +
                        "/v1/upload?access_token=5bbcf504ee5fd4bd6ef3b15d946a58c18bd5ce0e9177e&destinationPath=Prueba_SURA.txt&fileSize=0&allowOverwrite=0&resume=0", "uploadFile".getBytes()));


    }
}
