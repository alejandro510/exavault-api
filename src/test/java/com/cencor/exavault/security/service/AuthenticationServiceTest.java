package com.cencor.exavault.security.service;

import com.cencor.exavault.common.dto.ErrorResponse;
import com.cencor.exavault.security.dto.Login;
import com.cencor.exavault.common.dto.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceTest {
    @Mock
    private AuthenticationService authentication;

    private Response responseLoginTest;
    private Response responseIncorrectLoginTest;
    private Response responseIncorrectLogoutTest;

    @Before
    public void init() {
        responseLoginTest = new Response();
        Login loginTest = new Login();
        loginTest.setUsername("user");
        loginTest.setAccessToken("tokenLogin");

        responseLoginTest.setSuccess(1);
        responseLoginTest.setResults(loginTest);

        responseIncorrectLoginTest = new Response();
        ErrorResponse error = new ErrorResponse();
        error.setMessage("Invalid username or password");
        error.setCode(200);

        responseIncorrectLoginTest.setSuccess(0);
        responseIncorrectLoginTest.setError(error);

        responseIncorrectLogoutTest = new Response();
        ErrorResponse errorLogout = new ErrorResponse();
        errorLogout.setMessage("Access token 'tokenLogin' not found");
        errorLogout.setCode(310);

        responseIncorrectLogoutTest.setSuccess(0);
        responseIncorrectLogoutTest.setError(errorLogout);
    }

    @Test
    public void successLoginTest() {
        BDDMockito.when(authentication.login("api-key", "user", "password")).thenReturn(responseLoginTest);


        Response response = authentication.login("api-key", "user", "password");

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        Login login = objectMapper.convertValue (response.getResults(), Login.class);

        Assert.assertNotNull(login);
        Assert.assertEquals(1, response.getSuccess());
        Assert.assertEquals("user", login.getUsername());
        Assert.assertEquals("tokenLogin", login.getAccessToken());
    }

    @Test
    public void incorrectLoginTest() {
        BDDMockito.when(authentication.login("api-key", "user", "password")).thenReturn(responseIncorrectLoginTest);

        Response response = authentication.login("api-key", "user", "password");

        Assert.assertNotNull(response);

        ErrorResponse errorResponse = response.getError();

        Assert.assertNotNull(errorResponse);
        Assert.assertEquals(0, response.getSuccess());
        Assert.assertEquals("Invalid username or password", errorResponse.getMessage());
        Assert.assertEquals(200, errorResponse.getCode());
    }


    @Test
    public void successLogoutTest() {
        BDDMockito.when(authentication.logout("api-key", "tokenLogin")).thenReturn(responseLoginTest);

        Response response = authentication.logout("api-key", "tokenLogin");

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        Login login = objectMapper.convertValue (response.getResults(), Login.class);

        Assert.assertNotNull(login);
        Assert.assertEquals(1, response.getSuccess());
    }

    @Test
    public void incorrectLogoutTest() {
        BDDMockito.when(authentication.logout("api-key", "tokenLogin")).thenReturn(responseIncorrectLogoutTest);

        Response response = authentication.logout("api-key", "tokenLogin");

        Assert.assertNotNull(response);

        ErrorResponse errorResponse = response.getError();

        Assert.assertNotNull(errorResponse);
        Assert.assertEquals(0, response.getSuccess());
        Assert.assertEquals("Access token 'tokenLogin' not found", errorResponse.getMessage());
        Assert.assertEquals(310, errorResponse.getCode());
    }
}
