package com.cencor.exavault.security;

import com.cencor.exavault.common.dto.Response;
import com.cencor.exavault.security.dto.Login;
import com.cencor.exavault.security.service.AuthenticationService;
import com.cencor.exavault.security.service.AuthenticationServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthenticationServiceImpl.class})
@AutoConfigureWireMock(port = 0)
public class AuthenticationIntegrationTest {
    @Autowired
    AuthenticationService authenticationService;

    @Test
    public void loginTest() {
        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/authenticateUser"))
                        .withRequestBody(WireMock.equalTo("api_key=apiKey&username=user&password=password"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": {\"username\": \"user\",\"accessToken\": \"tokenLogin\",\"mode\": 0,\"clientIp\": \"172.16.2.234\"}}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        Response response = authenticationService.login("apiKey", "user", "password");

        Assert.assertNotNull(response);

        ObjectMapper objectMapper = new ObjectMapper();
        Login login = objectMapper.convertValue(response.getResults(), Login.class);

        Assert.assertNotNull(login);
        Assert.assertEquals(1, response.getSuccess());
        Assert.assertEquals("user", login.getUsername());
        Assert.assertEquals("tokenLogin", login.getAccessToken());
        Assert.assertEquals(0, login.getMode());
        Assert.assertEquals("172.16.2.234", login.getClientIp());

    }

    @Test
    public void logoutTest() {
        Response responseLoginTest = new Response();

        responseLoginTest.setSuccess(1);

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/logoutUser"))
                        .withRequestBody(WireMock.equalTo("api_key=apiKey&access_token=accessToken"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": []}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        Response response = authenticationService.logout("apiKey", "accessToken");

        Assert.assertNotNull(response);

        Assert.assertEquals(1, response.getSuccess());
    }

}
