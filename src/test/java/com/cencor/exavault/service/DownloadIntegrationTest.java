package com.cencor.exavault.service;

import com.cencor.exavault.common.exception.AuthenticationFailException;
import com.cencor.exavault.common.exception.FileNotFoundException;
import com.cencor.exavault.file.service.FileService;
import com.cencor.exavault.file.service.FileServiceImpl;
import com.cencor.exavault.security.service.AuthenticationService;
import com.cencor.exavault.security.service.AuthenticationServiceImpl;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ExavaultServiceImpl.class})
@AutoConfigureWireMock(port = 0)
public class DownloadIntegrationTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Value("${wiremock.server.port}")
    private String port;

    @Configuration
    static class ContextConfiguration {

        @Bean
        public AuthenticationService authenticationService() {
            return new AuthenticationServiceImpl();
        }

        @Bean
        public FileService fileService() {
            return new FileServiceImpl();
        }
    }

    @Autowired
    ExavaultService exavaultService;

    @Test
    public void downloadFileTest() throws UnsupportedEncodingException {
        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/authenticateUser"))
                        .withRequestBody(WireMock.equalTo("api_key=apiKey&username=user&password=password"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": {\"username\": \"user\",\"accessToken\": \"accessToken\",\"mode\": 0,\"clientIp\": \"172.16.2.234\"}}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        WireMock.stubFor(
                WireMock.get(
                        WireMock.urlEqualTo("/v1/getDownloadFileUrl?api_key=apiKey&access_token=accessToken&filePaths=filePath&downloadName=downloadName"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": {\"url\": \"http://localhost:" + port + "/downloadFile\"}}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        WireMock.stubFor(
                WireMock.get(
                        WireMock.urlEqualTo("/downloadFile"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withBody("downloadFile".getBytes())
                                        .withHeader("Content-Type", "application/octet-stream")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/logoutUser"))
                        .withRequestBody(WireMock.equalTo("api_key=apiKey&access_token=accessToken"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": []}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        byte[] file = exavaultService.downloadFile("apiKey", "user", "password", "filePath", "downloadName");

        Assert.assertNotNull(file);
        Assert.assertEquals("downloadFile", new String(file));
    }

    @Test
    public void apiKeyFailDownloadFileTest() throws UnsupportedEncodingException {
        expectedException.expect(AuthenticationFailException.class);
        expectedException.expectMessage("Error al autentificarse a Exavault, Api Key Incorrecto.");

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/authenticateUser"))
                        .withRequestBody(WireMock.equalTo("api_key=badApiKey&username=user&password=password"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\":0,\"error\":{\"message\":\"Invalid API key\",\"code\":250},\"results\":null}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        exavaultService.downloadFile("badApiKey", "user", "password", "filePath", "downloadName");
    }

    @Test
    public void authenticationFailDownloadFileTest() throws UnsupportedEncodingException {
        expectedException.expect(AuthenticationFailException.class);
        expectedException.expectMessage("Error al autentificarse a Exavault, usuario o password incorrectos.");

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/authenticateUser"))
                        .withRequestBody(WireMock.equalTo("api_key=apiKey&username=user&password=badPassword"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\":0,\"error\":{\"message\":\"Invalid username or password\",\"code\":200},\"results\":null}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        exavaultService.downloadFile("apiKey", "user", "badPassword", "filePath", "downloadName");
    }

    @Test
    public void fileNotFoundDownloadFileTest() throws UnsupportedEncodingException {
        expectedException.expect(FileNotFoundException.class);
        expectedException.expectMessage("El archivo errorFilePath no se encuentra en la ruta.");

        WireMock.stubFor(
                WireMock.post(
                        WireMock.urlEqualTo("/v1/authenticateUser"))
                        .withRequestBody(WireMock.equalTo("api_key=apiKey&username=user&password=password"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\": 1,\"error\": null,\"results\": {\"username\": \"user\",\"accessToken\": \"accessToken\",\"mode\": 0,\"clientIp\": \"172.16.2.234\"}}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        WireMock.stubFor(
                WireMock.get(
                        WireMock.urlEqualTo("/v1/getDownloadFileUrl?api_key=apiKey&access_token=accessToken&filePaths=errorFilePath&downloadName=downloadName"))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody("{\"success\":0,\"error\":{\"message\":\"The requested file does not exist: Report\\/SecLen_SURA_20181005.xm\",\"code\":450},\"results\":null}")
                                        .withStatus(HttpStatus.OK.value())
                        )
        );

        exavaultService.downloadFile("apiKey", "user", "password", "errorFilePath", "downloadName");
    }

}
